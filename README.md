Instalación: 

1.Git clone https://gitlab.com/lepfsd/sf3-api-prueba-strapp.git
2.cd sf3-api
3.composer install
4.Copiar el nombre de la bd en app\config\parameters.yml
5.Crear un nuevo schema MySql
6.php bin/console doctrine:schema:update --force
7.php bin/console server:run

Utilización

  1.Usar https://www.getpostman.com/ , https://insomnia.rest/, etc
. 2.Abrir en el explorador http://localhost:8000/api/doc para ver la documentación de la API
