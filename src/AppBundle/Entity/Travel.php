<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Travel
 *
 * @ORM\Table(name="travel")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TravelRepository")
 */
class Travel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="travelCode", type="string", length=255, nullable=true)
     */
    private $travelCode;

    /**
     * @var string
     *
     * @ORM\Column(name="seatNumber", type="string", length=255, nullable=true)
     */
    private $seatNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="destination", type="string", length=255, nullable=true)
     */
    private $destination;

    /**
     * @var string
     *
     * @ORM\Column(name="origin", type="string", length=255, nullable=true)
     */
    private $origin;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="string", length=255, nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(name="create_at", type="datetime", length=255, nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(name="update_at", type="datetime", length=255, nullable=true)
     */
    private $updatedAt;

    /**
     * @var AppBundle\Entity\Passenger
     *
     * @ORM\ManyToMany(targetEntity="Passenger")
     * @ORM\JoinTable(name="passengerTravel",
     *      joinColumns={@ORM\JoinColumn(name="travel", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="passenger", referencedColumnName="id")}
     *      )
     */
    private $passenger;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set travelCode
     *
     * @param string $travelCode
     *
     * @return Travel
     */
    public function setTravelCode($travelCode)
    {
        $this->travelCode = $travelCode;

        return $this;
    }

    /**
     * Get travelCode
     *
     * @return string
     */
    public function getTravelCode()
    {
        return $this->travelCode;
    }

    /**
     * Set seatNumber
     *
     * @param string $seatNumber
     *
     * @return Travel
     */
    public function setSeatNumber($seatNumber)
    {
        $this->seatNumber = $seatNumber;

        return $this;
    }

    /**
     * Get seatNumber
     *
     * @return string
     */
    public function getSeatNumber()
    {
        return $this->seatNumber;
    }

    /**
     * Set destination
     *
     * @param string $destination
     *
     * @return Travel
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * Get destination
     *
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Set origin
     *
     * @param string $origin
     *
     * @return Travel
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;

        return $this;
    }

    /**
     * Get origin
     *
     * @return string
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Travel
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    

    /**
     * Get the value of createdAt
     */ 
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set the value of createdAt
     *
     * @return  self
     */ 
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get the value of updatedAt
     */ 
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set the value of updatedAt
     *
     * @return  self
     */ 
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->passenger = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add passenger
     *
     * @param \AppBundle\Entity\Passenger $passenger
     *
     * @return Travel
     */
    public function addPassenger(\AppBundle\Entity\Passenger $passenger)
    {
        $this->passenger[] = $passenger;

        return $this;
    }

    /**
     * Remove passenger
     *
     * @param \AppBundle\Entity\Passenger $passenger
     */
    public function removePassenger(\AppBundle\Entity\Passenger $passenger)
    {
        $this->passenger->removeElement($passenger);
    }

    /**
     * Get passenger
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPassenger()
    {
        return $this->passenger;
    }
}
