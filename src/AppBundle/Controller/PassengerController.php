<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc; 
use AppBundle\Entity\Passenger;

class PassengerController extends Controller
{

    /**
     * @ApiDoc(
     *     resource=true,
     *     description="Get all passengers",
     *     requirements={
     *         {
     *             "name"="id",
     *             "dataType"="integer",
     *             "requirements"="\d+",
     *             "description"="The passenger unique identifier."
     *         }
     *     },
     *     section="passengers"
     * )
     * @Route("/api/passenger", name="list_passenger")
     * @Method({"GET"})
     */

    public function listPassenger()
    {
        //se obtienen todos los pasajeros
        $passengers = $this->getDoctrine()->getRepository('AppBundle:Passenger')
            ->findAll();
        if(!count($passengers)){
            $response = array(
                'code'=>1,
                'message'=>'No passengers found!',
                'errors'=>null,
                'result'=>null
            );
            return new JsonResponse($response, Response::HTTP_NOT_FOUND);
        }
        //trasformando a json el array de pasajeros
        $data = $this->get('jms_serializer')->serialize($passengers, 'json');
        $response=array(

            'code' => 0,
            'message' => 'success',
            'errors' => null,
            'result' => json_decode($data)

        );

        return new JsonResponse($response, 200);
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     description="Get one single passenger",
     *     requirements={
     *         {
     *             "name"="id",
     *             "dataType"="integer",
     *             "requirements"="\d+",
     *             "description"="The passenger unique identifier."
     *         }
     *     },
     *     section="passengers"
     * )
     * @Route("/api/passenger/{id}", name="show_passenger")
     * @Method({"GET"})
     */
    public function showPassenger($id)
    {
        $passenger = $this->getDoctrine()->getRepository('AppBundle:Passenger')
            ->find($id);

        if(empty($passenger)){
            $response = array(
                'code' => 1,
                'message' => 'passenger not found',
                'error' => null,
                'result' => null
            );
            return new JsonResponse($response, Response::HTTP_NOT_FOUND);
        }
        $data = $this->get('jms_serializer')->serialize($passenger, 'json');
      
        $response = array(
            'code' => 0,
            'message' => 'success',
            'error' => null,
            'result' => json_decode($data)
        );
        return new JsonResponse($response, 200);
    }

    /**
     * @ApiDoc(
     * description="Create a new passenger",
     *
     *    statusCodes = {
     *        200 = "Creation with success",
     *        400 = "invalid form"
     *    },
     *    responseMap={
     *         201 = {"class"=Passenger::class},
     *
     *    },
     *     section="passengers"
     * )
     * @param Request $request
     * @return JsonResponse
     * @Route("/api/passenger", name="create_passenger")
     * @Method({"POST"})
     */

     public function createPassenger(Request $request)
     {
        if(empty($request->request->all())){
            return new JsonResponse('Error', Response::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();

        //se recupera la peticion del frontend
        $identification = $request->request->get('identification');
        $name = $request->request->get('name');
        $address = $request->request->get('address');
        $phone = $request->request->get('phone');

        $passenger = new Passenger();

        // se setean los valores de acuerdo a los setters de la entidad
        $passenger->setIdentification($identification);
        $passenger->setName($name);
        $passenger->setAddress($address);
        $passenger->setPhone($phone);
        $passenger->setCreatedat(new \DateTime());
        $em->persist($passenger);
        $em->flush();
        $response = array(
            'code' => 0,
            'message' => 'Passenger created!',
            'errors' => null,
            'result' => null
        );

        return new JsonResponse($response, Response::HTTP_CREATED);
     }


    /**
     *@ApiDoc(
     * description="Update a new passenger",
     *
     *    statusCodes = {
     *        200 = "Update with success",
     *        400 = "invalid form"
     *    },
     *    responseMap={
     *         201 = {"class"=Passenger::class},
     *
     *    },
     *     section="passengers"
     *
     *
     * )
     * @param Request $request
     * @param $id
     * @Route("/api/passenger/{id}", name="update_passenger")
     * @Method({"PUT"})
     * @return JsonResponse
     */

    public function updatePassenger(Request $request, $id)
    {

        if(empty($request->request->all())){
            return new JsonResponse('Error', Response::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();

        $identification = $request->request->get('identification');
        $name = $request->request->get('name');
        $address = $request->request->get('address');
        $phone = $request->request->get('phone');

        // con doctrine se busca el objeto a actualizar
        $passenger = $this->getDoctrine()->getRepository('AppBundle:Passenger')->find($id);

        if($passenger !== null){
            $passenger->setIdentification($identification);
            $passenger->setName($name);
            $passenger->setAddress($address);
            $passenger->setPhone($phone);
            $passenger->setUpdatedAt(new \DateTime());
            
            $em->persist($passenger);
            $em->flush();
        }

        $response=array(

            'code'=>0,
            'message'=>'Passenger updated!',
            'errors'=>null,
            'result'=>null

        );

        return new JsonResponse($response,200);
    }

    /**
     * @ApiDoc(
     * description="Delete a new passenger",
     *
     *    statusCodes = {
     *        200 = "Delete with success",
     *        400 = "invalid form"
     *    },
     *    responseMap={
     *         201 = {"class"=Passenger::class},
     *
     *    },
     *     section="passengers"
     *
     *
     * )
     * 
     * @Route("/api/passenger/{id}", name="delete_passenger")
     * @Method({"DELETE"})
     */

    public function deletePassenger($id)
    {
        $passenger = $this->getDoctrine()->getRepository('AppBundle:Passenger')->find($id);

        if (empty($passenger)) {

            $response=array(

                'code' => 1,
                'message' => 'Passenger Not found !',
                'errors' => null,
                'result' => null

            );


            return new JsonResponse($response, Response::HTTP_NOT_FOUND);
        }

        $em=$this->getDoctrine()->getManager();
        $em->remove($passenger);
        $em->flush();
        $response=array(

            'code' => 0,
            'message' => 'passenger deleted !',
            'errors' => null,
            'result' => null

        );

        return new JsonResponse($response,200);
    }
}
