<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Entity\Passenger;
use AppBundle\Entity\Travel;

class TravelController extends Controller
{
    /**
     * @ApiDoc(
     *     resource=true,
     *     description="Get all travels",
     *     requirements={
     *         {
     *             "name"="id",
     *             "dataType"="integer",
     *             "requirements"="\d+",
     *             "description"="The travel unique identifier."
     *         }
     *     },
     *     section="travels"
     * )
     * @Route("/api/travel", name="list_travel")
     * @Method({"GET"})
     */

    public function listTravel()
    {
        //se obtienen todos los viajes
        $travels = $this->getDoctrine()->getRepository('AppBundle:Travel')
            ->findAll();
        if(!count($travels)){
            $response = array(
                'code'=>1,
                'message'=>'No travels found!',
                'errors'=>null,
                'result'=>null
            );
            return new JsonResponse($response, Response::HTTP_NOT_FOUND);
        }
        //trasformando a json el array de pasajeros
        $data = $this->get('jms_serializer')->serialize($travels, 'json');
        $response = array(

            'code' => 0,
            'message' => 'success',
            'errors' => null,
            'result' => json_decode($data)

        );
        return new JsonResponse($response, 200);
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     description="Get one single travel",
     *     requirements={
     *         {
     *             "name"="id",
     *             "dataType"="integer",
     *             "requirements"="\d+",
     *             "description"="The travel unique identifier."
     *         }
     *     },
     *     section="travels"
     * )
     * @Route("/api/travel/{id}", name="show_travel")
     * @Method({"GET"})
     */
    public function showTravel($id)
    {
        $travel = $this->getDoctrine()->getRepository('AppBundle:Travel')
            ->find($id);

        if(empty($travel)){
            $response = array(
                'code' => 1,
                'message' => 'travel not found',
                'error' => null,
                'result' => null
            );
            return new JsonResponse($response, Response::HTTP_NOT_FOUND);
        }
        $data = $this->get('jms_serializer')->serialize($travel, 'json');
        
        $response = array(
            'code' => 0,
            'message' => 'success',
            'error' => null,
            'result' => json_decode($data)
        );
        return new JsonResponse($response, 200);
    }

    /**
     * @ApiDoc(
     * description="Create a new travel",
     *
     *    statusCodes = {
     *        200 = "Creation with success",
     *        400 = "invalid form"
     *    },
     *    responseMap={
     *         201 = {"class"=Travel::class},
     *
     *    },
     *     section="travels"
     * )
     * @param Request $request
     * @return JsonResponse
     * @Route("/api/travel", name="create_travel")
     * @Method({"POST"})
     */

     public function createTravel(Request $request)
     {
          
        if(empty($request->request->all())){
            return new JsonResponse('Error', Response::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();
        //se recupera la peticion del frontend
        $travelCode = $request->request->get('travelCode');
        $seatNumber = $request->request->get('seatNumber');
        $destination = $request->request->get('destination');
        $origin = $request->request->get('origin');
        $price = $request->request->get('price');

        // se reciben los arrays de los pasajeros relacionados a los viajes de esta forma:
        // p_identification[], p_name[], p_address[], p_phone[]
        $p_identification = $request->request->get('p_identification');
        $p_name = $request->request->get('p_name');
        $p_address = $request->request->get('p_address');
        $p_phone = $request->request->get('p_phone');

        $travel = new Travel();

        $travel->setTravelCode($travelCode);
        $travel->setSeatNumber($seatNumber);
        $travel->setDestination($destination);
        $travel->setOrigin($origin);
        $travel->setPrice($price);
        $travel->setCreatedat(new \DateTime());
        
        if(count($p_identification)){

            for($i = 0; $i < count($p_identification); $i++){

                //en cada iteracion se almancena un nuevo objeto pasajero que pertenece a un viaje
                $passenger = new Passenger();
                $passenger->setIdentification($p_identification[$i]);
                $passenger->setName($p_name[$i]);
                $passenger->setAddress($p_address[$i]);
                $passenger->setPhone($p_phone[$i]);
                $passenger->setCreatedat(new \DateTime());
                $em->persist($passenger);
                $travel->addPassenger($passenger);
            }
        }

        $em->persist($travel);
        $em->flush();

        $response = array(
            'code' => 0,
            'message' => 'Travel created!',
            'errors' => null,
            'result' => null
        );

        return new JsonResponse($response, Response::HTTP_CREATED);
     }


    /**
     *
     * @ApiDoc(
     * description="Update a new travel",
     *
     *    statusCodes = {
     *        200 = "Update with success",
     *        400 = "invalid form"
     *    },
     *    responseMap={
     *         201 = {"class"=Travel::class},
     *
     *    },
     *     section="travels"
     *
     *
     * )
     * @param Request $request
     * @param $id
     * @Route("/api/travel/{id}", name="update_travel")
     * @Method({"PUT"})
     * @return JsonResponse
     */

    public function updateTravel(Request $request, $id)
    {

        $travel = $this->getDoctrine()->getRepository('AppBundle:Travel')->find($id);
        

        if(empty($request->request->all())){
            return new JsonResponse('Error', Response::HTTP_BAD_REQUEST);
        }
        
        $em = $this->getDoctrine()->getManager();
        $travelCode = $request->request->get('travelCode');
        $seatNumber = $request->request->get('seatNumber');
        $destination = $request->request->get('destination');
        $origin = $request->request->get('origin');
        $price = $request->request->get('price');

        $p_identification = $request->request->get('p_identification');
        $p_name = $request->request->get('p_name');
        $p_address = $request->request->get('p_address');
        $p_phone = $request->request->get('p_phone');

        $travel->setTravelCode($travelCode);
        $travel->setSeatNumber($seatNumber);
        $travel->setDestination($destination);
        $travel->setOrigin($origin);
        $travel->setPrice($price);
        $travel->setUpdatedAt(new \DateTime());

        // si vienen pasajeros del frontend se deben borrar los existentes para almanecar los nuevos
        if(count($p_identification)){
            $passengers_old = $travel->getPassenger();
            if($passengers_old !== null){
                foreach($passengers_old as $item){
                    $travel->removePassenger($item);
                    $em->flush();
                }
            }
            
            for($i = 0; $i < count($p_identification); $i++){
                $passenger = new Passenger();
                $passenger->setIdentification($p_identification[$i]);
                $passenger->setName($p_name[$i]);
                $passenger->setAddress($p_address[$i]);
                $passenger->setPhone($p_phone[$i]);
                $passenger->setCreatedat(new \DateTime());
                $em->persist($passenger);
                $travel->addPassenger($passenger);
            }
        }

        $em->persist($travel);
        $em->flush();

        $response=array(

            'code'=>0,
            'message'=>'Travel updated!',
            'errors'=>null,
            'result'=>null

        );

        return new JsonResponse($response,200);
    }

    /**
     * @ApiDoc(
     * description="Delete a new travel",
     *
     *    statusCodes = {
     *        200 = "Delete with success",
     *        400 = "invalid form"
     *    },
     *    responseMap={
     *         201 = {"class"=Travel::class},
     *
     *    },
     *     section="travels"
     *
     *
     * )
     * @Route("/api/travel/{id}", name="delete_travel")
     * @Method({"DELETE"})
     */

    public function deleteTravel($id)
    {
        $travel = $this->getDoctrine()->getRepository('AppBundle:Travel')->find($id);
        $em = $this->getDoctrine()->getManager();

        if (empty($travel)) {

            $response=array(

                'code' => 1,
                'message' => 'Travel Not found !',
                'errors' => null,
                'result' => null

            );

            return new JsonResponse($response, Response::HTTP_NOT_FOUND);
        }
        $passengers_old = $travel->getPassenger();
        if($passengers_old !== null){
            foreach($passengers_old as $item){
                $travel->removePassenger($item);
                $em->flush();
            }
        }

        $em->remove($travel);
        $em->flush();
        $response = array(

            'code' => 0,
            'message' => 'Travel deleted !',
            'errors' => null,
            'result' => null

        );

        return new JsonResponse($response,200);
    }
}
